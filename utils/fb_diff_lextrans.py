#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# vim: set fileencoding=utf-8
#
# fb_diff_lextrans.py: a auxiliary script to create a new lexicon including 
# the words that were not part of the AM training data.
#
# Grupo FalaBrasil (2019)
# Universidade Federal do Pará
#
# author: apr 2019
# cassio batista - cassio.batista.13@gmail.com

import sys
import icu
import os
os.environ['CLASSPATH'] = os.path.join(os.getenv('HOME'), 
			'fb-gitlab', 'fb-nlp', 'nlp-generator', # NOTE: call before jni
			'fb_nlplib.jar')

from jnius import autoclass

# TODO: make it return an int > 0 for exiting, 0 for success
def check_inputfile(filename):
	if not os.path.isfile(filename):
		print('error: input file "%s" does not exist' % filename)
		sys.exit(1)
	elif not filename.endswith('.txt'):
		print('error: input file "%s" does not have ".txt" extension' % filename)
		sys.exit(1)
	# https://stackoverflow.com/questions/2507808/how-to-check-whether-a-file-is-empty-or-not
	elif not os.stat(filename).st_size:
		print('error: input file "%s" appears to be empty' % filename)
		sys.exit(1)

class FalaBrasilNLP:
	def __init__(self):
		super(FalaBrasilNLP, self).__init__()
		self.jClass = autoclass('ufpa.util.PyUse')()

	def get_g2p(self, word):
		return self.jClass.useG2P(word)

	def get_syl(self, word):
		return self.jClass.useSyll(word)

	def get_stressindex(self, word):
		return self.jClass.useSVow(word)

	def get_g2psyl(self, word):
		return self.jClass.useSG2P(word)

	def print_asciilogo(self):
		print('\033[94m  ____                         \033[93m _____     _           \033[0m')
		print('\033[94m / ___| _ __ _   _ _ __   ___  \033[93m|  ___|_ _| | __ _     \033[0m')
		print('\033[94m| |  _ | \'__| | | | \'_ \ / _ \ \033[93m| |_ / _` | |/ _` |  \033[0m')
		print('\033[94m| |_| \| |  | |_| | |_) | (_) |\033[93m|  _| (_| | | (_| |    \033[0m')
		print('\033[94m \____||_|   \__,_| .__/ \___/ \033[93m|_|  \__,_|_|\__,_|    \033[0m')
		print('                  \033[94m|_|      \033[32m ____                _ _\033[91m  _   _ _____ ____    _            \033[0m')
		print('                           \033[32m| __ ) _ __ __ _ ___(_) |\033[91m| | | |  ___|  _ \  / \          \033[0m')
		print('                           \033[32m|  _ \| \'_ / _` / __| | |\033[91m| | | | |_  | |_) |/ ∆ \          \033[0m')
		print('                           \033[32m| |_) | | | (_| \__ \ | |\033[91m| |_| |  _| |  __// ___ \        \033[0m')
		print('                           \033[32m|____/|_|  \__,_|___/_|_|\033[91m \___/|_|   |_|  /_/   \_\       \033[0m')
		print('')

if __name__=='__main__':
	fb  = FalaBrasilNLP()
	if len(sys.argv) != 3:
		fb.print_asciilogo()
		print('usage: (python3) %s <trans.txt> <lexicon.txt>' % sys.argv[0])
		print('\t<trans.txt>')
		print('\t<lexicon.txt>')
		sys.exit(1)
	f= open("/home/larissa/guru99.txt","w+")
	# process trans input file
	trans_filename = sys.argv[1]
	check_inputfile(trans_filename)

	# process lexicon ids input file
	lexicon_filename = sys.argv[2]
	check_inputfile(lexicon_filename)

	with open (trans_filename, 'r') as trans:
		trans_wlist = trans.read().split()

	lex_wlist = []
	lex_contents = []
	with open (lexicon_filename, 'r') as lex:
		for entry in lex:
			lex_contents.append(entry)
			if '\t' in entry:
				lex_wlist.append(entry.split('\t')[0])

	print('words to be appended to lexicon:', end=' ')
	graphemes = set()
	for word in trans_wlist:
		if word not in lex_wlist:
			print('"%s"' % word, end=',')
			graphemes.add(word)

	if len(graphemes) == 0:
		print(None)
	else:
		print()

	for word in graphemes:
		phonemes = fb.get_g2p(word)
		lex_contents.append(word + '\t' + phonemes + '\n')

	collator = icu.Collator.createInstance(icu.Locale('pt_BR.UTF-8'))
	lex_contents = sorted(lex_contents, key=collator.getSortKey)
	with open(lexicon_filename, 'w') as lex:
		for entry in lex_contents:
			lex.write(entry)
	print('done!')
### EOF ###
