#!/bin/bash
#
# Grupo FalaBrasil (2019)
# Universidade Federal do Pará (UFPA)
#
# author: may 2019
# Ana Larissa Dias - larissa.engcomp@gmail.com

#https://groups.google.com/forum/#!topic/kaldi-help/37aE10tyyxI

kaldi_path=/home/larissa/kaldi
local=data/local
lang=data/lang
txt=/home/larissa/dataset/male_resampled/M-002.txt
work_dir=$kaldi_path/egs/novo-2
lexicon=$work_dir/data/local/dict/lexicon.txt

#edit the lexicon.txt - chamar script do cassio
#fb_diff_lextrans.py
#print('usage: (python3) %s <trans.txt> <lexicon.txt>' % sys.argv[0])
python3 fb_diff_lextrans.py $txt $lexicon 

#prepare_lang.sh add the --phone-symbol-table option giving it the phones.txt from the lang directory
utils/prepare_lang.sh data/local/dict "<UNK>" data/local/lang data/lang

#re-compile the G.fst
$kaldi_path/src/lmbin/arpa2fst --disambig-symbol=#0 --read-symbol-table=$lang/words.txt $local/tmp/lm.arpa $lang/G.fst

### EOF ###
