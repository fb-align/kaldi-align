#!/bin/bash
#
# Grupo FalaBrasil (2019)
# Universidade Federal do Pará (UFPA)
#
# author: may 2019
# Ana Larissa Dias - larissa.engcomp@gmail.com

work_dir=/home/larissa/UFPAlign
wav_dir=/home/larissa/fb-gitlab/fb-audio-corpora/male-female-aligned/male/*.wav
txt_dir=/home/larissa/fb-gitlab/fb-audio-corpora/male-female-aligned/male/*.txt
#wav_dir=/home/larissa/dataset/female/*.wav
#txt_dir=/home/larissa/dataset/female/*.txt
kaldi_path=/home/larissa/kaldi
output_dir=/home/larissa/UFPAlign/textgrid
corpus=/home/larissa/UFPAlign/data/corpus.txt

#ac_mono=/media/larissa/kaldi_new/cluster/novo-2/exp/mono
#ac_tri1=/media/larissa/kaldi_new/cluster/novo-2/exp/tri1_8k-16
#ac_tri2=/media/larissa/kaldi_new/cluster/novo-2/exp/tri2_8k-16
#ac_tri3=/media/larissa/kaldi_new/cluster/novo-2/exp/tri3_8k-16
#ac_dnn=/media/larissa/kaldi_new/cluster/novo-2/exp/nnet4d_tri3_8k-8
#lang_dir=/media/larissa/kaldi_new/cluster/novo-2/data/lang
nj=1

ac_mono=/media/larissa/kaldi_new/kaldi/egs/bracis/exp/mono
ac_tri1=/media/larissa/kaldi_new/kaldi/egs/bracis/exp/tri1
ac_tri2=/media/larissa/kaldi_new/kaldi/egs/bracis/exp/tri2b
ac_tri3=/media/larissa/kaldi_new/kaldi/egs/bracis/exp/tri3b
ac_dnn=/media/larissa/kaldi_new/kaldi/egs/bracis/exp/dnn_bracis
ac_dnn=/media/larissa/kaldi_new/kaldi/egs/bracis/exp/dnn_iberspeech
lang_dir=/media/larissa/kaldi_new/kaldi/egs/bracis/data/lang

local=data/local
lang=data/lang
#work_dir=/media/larissa/kaldi_new/kaldi/egs/bracis
lexicon=data/local/dict/lexicon.txt

# Extend the lang if needed
function extend_lang(){
	#fazer cópia do data/local/dict e data/local/tmp. Excluir o lexiconp.txt data/lang data/local/lang
	#mkdir data
	#cp -r /media/larissa/kaldi_new/kaldi/egs/bracis/data_original/local/ /media/larissa/kaldi_new/kaldi/egs/bracis/data/local
	for file in ${1}; do
		python3 fb_diff_lextrans.py $file ${2}
	done
	utils/prepare_lang.sh data/local/dict "<UNK>" data/local/lang data/lang
	src/lmbin/arpa2fst --disambig-symbol=#0 --read-symbol-table=${3}/words.txt ${4}/tmp/lm.arpa ${3}/G.fst
}

# Create wav.scp
function create_wav() {
	mkdir -p ${1}/data
	> ${1}/data/wav.scp
	for f in ${2}; do
		bf=`basename $f`
        	bf=${bf%.wav}
        	echo $bf $f >> ${1}/data/wav.scp
	done
}

# Create text
function create_text() {
	for file in ${2}
	do
  		#echo "Processing $file file..."
  		cat $file >> ${1}/data/corpus.txt
	done
	cut -d ' ' -f1 ${1}/data/wav.scp >> ${1}/data/id_spk.tmp
	paste -d ' ' ${1}/data/id_spk.tmp ${1}/data/corpus.txt > ${1}/data/text
}

# Create utt2spk, and spk2utt
function create_utt2spk_spk2utt() {
	awk -F" " '{print $1,$1}' ${1}/data/wav.scp  >> ${1}/data/utt2spk
	${2}/egs/wsj/s5/utils/utt2spk_to_spk2utt.pl ${1}/data/utt2spk > ${1}/data/spk2utt
}


# Extracting MFCC features
function extract_mfcc(){
	mfccdir=${1}/mfcc
    	${2}/egs/wsj/s5/utils/fix_data_dir.sh ${1}/data # tool for data proper sorting if needed - here: for data/train directory
    	${2}/egs/wsj/s5/steps/make_mfcc.sh --cmd run.pl --nj $nj ${1}/data ${1}/make_mfcc $mfccdir
    	${2}/egs/wsj/s5/utils/fix_data_dir.sh ${1}/data
    	${2}/egs/wsj/s5/steps/compute_cmvn_stats.sh ${1}/data ${1}/make_mfcc $mfccdir
    	${2}/egs/wsj/s5/utils/fix_data_dir.sh ${1}/data
}


# Forced alignment
function align_si() {
	${1}/egs/wsj/s5/steps/align_si.sh --nj ${2} --cmd run.pl ${3}/data ${4} ${5} ${3}/${6}_ali 
}

function align_fmllr(){
       ${1}/egs/wsj/s5/steps/align_fmllr.sh --nj ${2} --cmd run.pl ${3}/data ${4} ${5} ${3}/${6}_ali 
}

function align_dnn(){
	${1}/egs/wsj/s5/steps/nnet2/align.sh --nj ${2} --cmd run.pl ${3}/data ${4} ${5} ${3}/${6}_ali 
}


# Creates ctm files
function create_ctm() {
	# creates phonesid.ctm
	for i in  ${1}/${5}_ali/ali.*.gz;
		do ${2}/src/bin/ali-to-phones --ctm-output ${3}/final.mdl \
		ark:"gunzip -c $i|" -> ${i%.gz}.ctm;
	done;
	cat ${1}/${5}_ali/*.ctm > ${1}/ctm/${5}.phoneids.ctm

	#  creates grapheme.ctm
	${2}/egs/wsj/s5/steps/get_train_ctm.sh ${1}/data ${4} ${1}/${5}_ali ${1}/ctm_tmp
	cat ${1}/ctm_tmp/ctm > ${1}/ctm/${5}.graphemes.ctm
	rm -r ${1}/ctm_tmp
}

# Converte ctm to textgrid  (python3) fb_ctm2textgrid.py <file.graphemes.ctm> <file.phoneids.ctm> <out_dir>' 
function ctm_to_textgrid(){
        mkdir -p ${1}/${3}
	python3 local/fb_ctm2textgrid.py ${2}/ctm/${3}.graphemes.ctm ${2}/ctm/${3}.phoneids.ctm ${1}/${3}
}


(extend_lang "$txt_dir" "$lexicon" "$lang" "$local")&
sleep 1
echo -e "\033[1mextended lang...\033[0m"
wait

# Create text, wav.scp, utt2spk and spk2utt.
(create_wav "$work_dir" "$wav_dir")&
sleep 1
echo -e "\033[1mcreated wav.scp file...\033[0m"
(create_text "$work_dir" "$txt_dir")&
sleep 1

echo -e "\033[1mcreated text file...\033[0m"
(create_utt2spk_spk2utt "$work_dir" "$kaldi_path")&
sleep 1
echo -e "\033[1mcreated utt2spk and spk2utt files...\033[0m"
#rm $work_dir/data/*.tmp

# Extract MFCC features
(extract_mfcc "$work_dir" "$kaldi_path")&
sleep 1
echo -e "\033[1mextracted MFCC features...\033[0m"
sleep 1

: '
# Forced alignment
(align_si "$kaldi_path" "$nj" "$work_dir" "$lang_dir" "$ac_mono"  "mono")&
sleep 1
echo -e "\033[1mmono alignment...\033[0m"
(align_si "$kaldi_path" "$nj" "$work_dir" "$lang_dir" "$ac_tri1"  "tri1")&
sleep 1
echo -e "\033[1mtri1 alignment...\033[0m"
(align_si "$kaldi_path" "$nj" "$work_dir" "$lang_dir" "$ac_tri2"  "tri2")&
sleep 1
echo -e "\033[1mtri2 alignment...\033[0m"
(align_fmllr "$kaldi_path" "$nj" "$work_dir" "$lang_dir" "$ac_tri3"  "tri3")&
sleep 1
echo -e "\033[1mtri3 alignment...\033[0m"
(align_dnn "$kaldi_path" "$nj" "$work_dir" "$lang_dir" "$ac_dnn"  "dnn")&
sleep 1
echo -e "\033[1mDNN alignment...\033[0m"
'

# Forced alignment
echo
echo "===== MONO ALIGMENT ====="
echo

$kaldi_path/egs/wsj/s5/steps/align_si.sh --nj $nj --cmd run.pl $work_dir/data $lang_dir $ac_mono $work_dir/mono_ali
sleep 1

echo                                                                                                                                                       
echo "===== TRI1(first triphone pass) DELTA FEATURES ALIGNMENT ====="
echo

$kaldi_path/egs/wsj/s5/steps/align_si.sh --nj $nj --cmd run.pl  $work_dir/data $lang_dir $ac_tri1 $work_dir/tri1_ali
sleep 1

echo                                                                                                                                                       
echo "===== TRI2 (delta + delta-delta) ALIGNMENT ====="
echo

$kaldi_path/egs/wsj/s5/steps/align_si.sh --nj $nj --cmd run.pl  $work_dir/data $lang_dir $ac_tri2 $work_dir/tri2b_ali
sleep 1

echo                                                                                                                                                       
echo "===== TRI3 (LDA-MLLT with FMLLR) ALIGNMENT ====="
echo

$kaldi_path/egs/wsj/s5/steps/align_fmllr.sh --nj $nj --cmd run.pl  $work_dir/data $lang_dir $ac_tri3 $work_dir/tri3b_ali
sleep 1

echo                                                                                                                                                       
echo "===== DNN ALIGNMENT ====="
echo

$kaldi_path/egs/wsj/s5/steps/nnet2/align.sh --nj $nj --cmd run.pl  $work_dir/data $lang_dir $ac_dnn $work_dir/dnn_bracis_ali
$kaldi_path/egs/wsj/s5/steps/nnet2/align.sh --nj $nj --cmd run.pl  $work_dir/data $lang_dir $ac_dnn $work_dir/dnn_iberspeech_ali

sleep 1


# Creates CTM files
mkdir $work_dir/ctm
(create_ctm "$work_dir" "$kaldi_path" "$ac_mono" "$lang_dir" "mono")&
sleep 1
echo -e "\033[1mCreated CTM files for monophone acoustic model...\033[0m"
(create_ctm "$work_dir" "$kaldi_path" "$ac_tri1" "$lang_dir" "tri1")&
sleep 1
echo -e "\033[1mCreated CTM files for triphone delta acoustic model...\033[0m"
(create_ctm "$work_dir" "$kaldi_path" "$ac_tri2" "$lang_dir" "tri2b")&
sleep 1
echo -e "\033[1mCreated CTM files for triphone delta+delta-delta acoustic model...\033[0m"
(create_ctm "$work_dir" "$kaldi_path" "$ac_tri3" "$lang_dir" "tri3b")&
sleep 1
echo -e "\033[1mCreated CTM files for triphone LDA-MLLT acoustic model...\033[0m"
(create_ctm "$work_dir" "$kaldi_path" "$ac_dnn" "$lang_dir" "dnn_iberspeech")&
sleep 1
echo -e "\033[1mCreated CTM files for DNN acoustic model...\033[0m"
(create_ctm "$work_dir" "$kaldi_path" "$ac_dnn" "$lang_dir" "dnn_bracis")&
sleep 1
echo -e "\033[1mCreated CTM files for DNN acoustic model...\033[0m"

# converte ctm to textgrid  (python3) %s <file.graphemes.ctm> <file.phoneids.ctm> <out_dir>' 
(ctm_to_textgrid "$output_dir" "$work_dir" "mono")&
sleep 1
echo -e "\033[1converted ctm to textgrid...\033[0m"
(ctm_to_textgrid "$output_dir" "$work_dir" "tri1")&
sleep 1
echo -e "\033[1converted ctm to textgrid...\033[0m"
(ctm_to_textgrid "$output_dir" "$work_dir" "tri2b")&
sleep 1
echo -e "\033[1converted ctm to textgrid...\033[0m"
(ctm_to_textgrid "$output_dir" "$work_dir" "tri3b")&
sleep 1
echo -e "\033[1converted ctm to textgrid...\033[0m"
(ctm_to_textgrid "$output_dir" "$work_dir" "dnn_iberspeech")&
sleep 1
echo -e "\033[1converted ctm to textgrid...\033[0m"
(ctm_to_textgrid "$output_dir" "$work_dir" "dnn_bracis")&
sleep 1
echo -e "\033[1converted ctm to textgrid...\033[0m"



### EOF ###
