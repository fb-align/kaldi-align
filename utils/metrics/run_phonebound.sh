#!/bin/bash
#
# authos: may 2020
# cassio batista - https://cassota.gitlab.io/

base_dir=${HOME}/fb-gitlab/fb-audio-corpora/male-female-aligned/male/
truth_dir=$base_dir/textgrid_falabrasil/
align_dirs=(
    $base_dir/18_iberspeech_kaldi/00mono/
    $base_dir/18_iberspeech_kaldi/01tri1/
    $base_dir/18_iberspeech_kaldi/02tri2/
    $base_dir/18_iberspeech_kaldi/03tri3/
    $base_dir/18_iberspeech_kaldi/04nnet2_2hidden/
    $base_dir/20_bracis_kaldi/00mono/
    $base_dir/20_bracis_kaldi/01tri1_delta/
    $base_dir/20_bracis_kaldi/02tri2_mllt/
    $base_dir/20_bracis_kaldi/03tri3_sat/
    $base_dir/20_bracis_kaldi/04nnet2_2hidden/
    $base_dir/20_bracis_kaldi/04nnet2_4hidden/
)

res_dir=../../results/male
mkdir -p $res_dir

start_date=$(date)
for align_dir in ${align_dirs[@]} ; do
    log_file="$res_dir/$(basename $(dirname $align_dir))_$(basename $align_dir).pb"
    (./phonebound.sh $truth_dir $align_dir $log_file)&
    sleep .1
    echo "$0: $log_file"
done

for pid in $(jobs -p) ; do
    wait $pid
done

echo "$0: done!"
echo "$0: $start_date"
echo "$0: $(date)"
