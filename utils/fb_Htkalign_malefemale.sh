#!/bin/bash
#
# Grupo FalaBrasil (2019)
# Universidade Federal do Pará (UFPA)
#
# author: may 2019
# Ana Larissa Dias - larissa.engcomp@gmail.com

#wav_dir=/home/larissa/male/*.wav
#txt_dir=/home/larissa/male/*.txt
wav_dir=/home/larissa/female/*.wav
txt_dir=/home/larissa/female/*.txt



# Create wav.list
function create_wav() {
	#mkdir -p ${1}/data
	for wav in ${2}; do
		bf=`basename $wav`
        	bf=${bf%.wav}
        	echo $wav >> temp/${bf}_wav_list
	done
}

# Create txt.list
function create_text() {
	for txt in ${2}; do
		bf=`basename $txt`
        	bf=${bf%.txt}
        	echo $txt >> temp/${bf}_txt_list
	done
}


# Create text, wav.scp, utt2spk and spk2utt.
(create_wav "$work_dir" "$wav_dir"  "$txt_dir")&
sleep 1
echo -e "\033[1mcreated wav_list file...\033[0m"
(create_text "$work_dir" "$txt_dir")&
echo -e "\033[1mcreated txt_list file...\033[0m"
sleep 1


temp=/home/larissa/temp/*_txt_list
for file in $temp; do echo "$(cat $(cat $file) | sed 's/ /\n/g')" > $(cat $file | sed 's/\.txt/\.lab/g'); done
echo -e "\033[1mcreated .lab file...\033[0m"
sleep 1


for file in $temp; do
	bf=`basename $file`
        bf=${bf%_txt_list}
	cat `cat $file` | sed 's/ /\n/g' >> temp/${bf}_word_list
	cat temp/${bf}_word_list | sort | uniq > temp/${bf}_word_list.temp
	mv temp/${bf}_word_list.temp temp/${bf}_word_list
	# Criando o dicionário usando o g2p
	mono g2p/lapsg2p.exe -w temp/${bf}_word_list -d temp/${bf}_dictionary.temp  
	# Adicionando sil no dicionário
	echo '<s> sil' > temp/${bf}_dictionary
	echo '</s> sil' > temp/${bf}_dictionary
	echo '<unk> sil' > temp/${bf}_dictionary
	cat temp/${bf}_dictionary.temp >> temp/${bf}_dictionary 
	rm -f temp/${bf}_dictionary.temp
	echo "sil sil" >> temp/${bf}_dictionary
	sleep 1
	sleep 1
	# Rodando o HVite para gerar o arquivo alinhado
	HVite -A -t 1 -b sil -a -m -C confs/comp.cfg -H model/hmmdefs -t 250.0 temp/${bf}_dictionary util/phones.list $(cat temp/${bf}_wav_list)
	python util/convert.py -r $(cat temp/${bf}_wav_list | sed 's/\.wav/\.rec/g') -t $(cat temp/${bf}_wav_list  | sed 's/\.wav/\.TextGrid/g') 
done

### EOF ###
