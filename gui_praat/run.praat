
# Iniciando o label:
#runSystem: "bash teste.sh >> ~/log3"
label SCREEN

# Design da janela Demo   
	demo Erase all
	demo Select inner viewport... 0 100 0 100
	demo Axes... 0 100 0 100

	# Título 
	demo Colour... 0
	demo Text special... 50 centre 95 half Times 20 0 UFPAlign 2.0
   
	demo Colour... 0
	demo Text special... 20 centre 85  half Times 15 0 Caminho Kaldi:
	demo Colour... 0
	demo Text special... 20 centre 75  half Times 15 0 Arquivo de áudio (WAV):
	demo Colour... 0
	demo Text special... 20 centre 65 half Times 15 0 Transcrição do áudio (TXT): 
	#demo Colour... 0
	#demo Text special... 20 centre 55 half Times 15 0 Salvar em:

	# Botão para escolher caminho Kaldi
	demo Black
	demo Line width... 2
	demo Draw rounded rectangle... 35 45 83 88 2
	demo Colour... 0
	demo Text special... 40 centre 85.5 half Times 15 0 Procurar...

	# Botão para procurar arquivo .wav
	demo Black
	demo Line width... 2
	demo Draw rounded rectangle... 35 45 73 78 2
	demo Colour... 0
	demo Text special... 40 centre 75.5 half Times 15 0 Procurar...

	# Botão para procurar arquivo .txt
	demo Black
	demo Line width... 2
	demo Draw rounded rectangle... 35 45 63 68 2
	demo Colour... 0
	demo Text special...  40 centre 65.5 half Times 15 0 Procurar...
	
	# Botão para iniciar o alinhamento
	demo Black
	demo Line width... 2
	demo Draw rounded rectangle... 35 45 53 58 2
	demo Colour... 0
	demo Text special... 40 centre 55.5 half Times 15 0 Alinhar
	

	# Esperando entrada do usuário
	while demoWaitForInput ()
			if demoClickedIn (35, 45, 83, 88)
				kaldi_path$ = chooseDirectory$ ("Escolha o caminho do Kaldi:")
				kaldi_egs$ = kaldi_path$ + "/egs"
				kaldi_project$ = kaldi_path$ + "/egs/UFPAlign"
				kaldi_path$ > temp/kaldi_path
				kaldi_project$ > temp/kaldi_project
				kaldi_egs$ > temp/kaldi_egs
				
				goto SCREEN

			# If para verificar o clic no botão "procurar" para o arquivo wav
			# A variável wavFileName salva o caminho do arquivo de áudio
			elsif demoClickedIn (35, 45, 73, 78) 
				wavFileName$ = chooseReadFile$ ("Escolha o arquivo de áudio:")
				wavFileName$ > temp/wav_list
				goto SCREEN

			# If para verificar o clic no botão "procurar" para o arquivo txt
			# A variável txtFileName salva o caminho do arquivo de transcrição do áudio
			elsif demoClickedIn (35, 45, 63, 68)
				txtFileName$ = chooseReadFile$ ("Escolha a transcrição do áudio:")
				txtFileName$ > temp/txt_list
				goto SCREEN

			# If para verificar o clic no botão "alinhar"
			elsif demoClickedIn (35, 45, 53, 58)
				demo Paint rounded rectangle... red 35 45 53 58 2
				system tar xvzf UFPAlign.tar.gz -C $( cat temp/kaldi_egs )
				system cp -r temp $( cat temp/kaldi_project )
				#system cd $( cat temp/kaldi_project ) && bash fb_kaldialign.sh $( cat temp/kaldi_path ) $( cat temp/kaldi_project ) $( cat temp/wav_list ) $( cat temp/txt_list )
				runSystem: "cd $( cat temp/kaldi_project ) && bash fb_kaldialign.sh $( cat temp/kaldi_path ) $( cat temp/kaldi_project ) $( cat temp/wav_list ) $( cat temp/txt_list ) >> ~/log"
			goto SCREEN2
			else
				goto SCREEN
			endif


label SCREEN2
	demo Erase all
	demo Select inner viewport... 0 100 0 100
	demo Axes... 0 100 0 100

	# Título 
	demo Colour... 0
	demo Text special... 50 centre 95 half Times 20 0 UFPAlign
	
	demo Colour... 0
	demo Text special... 50 centre 85  half Times 15 0 Seu arquivo foi alinhado com sucesso!
	demo Colour... 0
	demo Text special... 20 centre 75 half Times 15 0 Deseja abrir o arquivo alinhado?
	
	# Botão para procurar arquivo .txt
	demo Red
	demo Line width... 2
	demo Draw rounded rectangle... 35 45 73 78 2
	demo Colour... {0.8,0.1,0.2}
	demo Text special...  40 centre 75.5 half Times 15 0 Sim

	demo Red
	demo Line width... 2
	demo Draw rounded rectangle...  48 58 73 78 2
	demo Colour... {0.8,0.1,0.2}
	demo Text special... 53 centre 75.5 half Times 15 0 Não

	while demoWaitForInput ()

		# If para verificar o clic no botão "procurar" para o arquivo wav
		# A variável wavFileName salva o caminho do arquivo de áudio
		if demoClickedIn (35, 45, 73, 78)
				wavFileName$ < temp/wav_list
				tgFileName$ < temp/tg_list
				prefix$ < temp/prefix
				Read from file... 'tgFileName$'
				nametg$ = selected$("TextGrid")
				Read from file... 'wavFileName$'
				namesound$ = selected$("Sound")
				select Sound 'namesound$'
				plus TextGrid 'nametg$'
				Edit
				goto SCREEN
		else
				goto SCREEN2

label END

