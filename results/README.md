# Results for BRACIS 2020 Paper

This repo contains the raw values over which the results for our 2020 BRACIS
paper were calculated. The phonetic boundary metric was calculated using the
male-female dataset, which was manually aligned by some real phonetician. It
can be found at https://gitlab.com/fb-audio-corpora/male-female-aligned, but
you'll only have access if you're a member of the FalaBrasil Group.

Only the male dataset was used for techcnical reason, that's why there's only
the male folder here. The `*.pb` files contain the phonetic boudnary per token
(phoneme), which were calculated using `phonebound.sh` script (see
`utils/metrics/` dir for details).

```bash
$ tree male/
male/
├── 16_propor_htk_gmm.pb
├── 18_iberspeech_kaldi_00mono.pb
├── 18_iberspeech_kaldi_01tri1.pb
├── 18_iberspeech_kaldi_02tri2.pb
├── 18_iberspeech_kaldi_03tri3.pb
├── 18_iberspeech_kaldi_04nnet2_2hidden.pb
├── 20_bracis_kaldi_00mono.pb
├── 20_bracis_kaldi_01tri1_delta.pb
├── 20_bracis_kaldi_02tri2_mllt.pb
├── 20_bracis_kaldi_03tri3_sat.pb
├── 20_bracis_kaldi_04nnet2_2hidden.pb
└── 20_bracis_kaldi_04nnet2_4hidden.pb
```

[![FalaBrasil](../doc/logo_fb_github_footer.png)](https://ufpafalabrasil.gitlab.io/ "Visite o site do Grupo FalaBrasil") [![UFPA](../doc/logo_ufpa_github_footer.png)](https://portal.ufpa.br/ "Visite o site da UFPA")

__Grupo FalaBrasil (2020)__ - https://ufpafalabrasil.gitlab.io/      
__Universidade Federal do Pará (UFPA)__ - https://portal.ufpa.br/     
Cassio Batista - https://cassota.gitlab.io/    
