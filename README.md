# Scripts for Phonetic Alignment with Kaldi

## Requirements

- [Kaldi](https://kaldi-asr.org/doc/install.html)
- [Praat](http://www.fon.hum.uva.nl/praat/)
- Python 3 
- JDK
- [Cython](https://cython.org/)
- [PyICU](https://pypi.org/project/PyICU/)
- [PyJNIus](https://pyjnius.readthedocs.io/en/stable/)

## Installation

To install this plugin, download the `plugin_ufpalign-2.0.tar.gz` and extract
it in your Praat's preference folder (`~/.praat`). Once the plugin folder is
placed in Praat's preference folder, starting Praat will automatically add the
aligner tool to the `new` menu of Praat.

## Scripts

- `fb_addnewwords.sh`: a script to extend the LM with new words from the target
   dataset we are trying to align that were not part of the AM training data.    
- `fb_ctm2textgrid.py`: a script to convert CTM files from Kaldi aligner to
   Praat's.    
- `fb_diff_lextrans.py`: a auxiliary script to create a new lexicon including
   the words that were not part of the AM training data.    
- `fb_kaldialign.sh`: a script to compute forced alignment using Kaldi toolkit.     
- `fb_kaldialign_malefemale.sh`: a script to compute forced alignment using
   Kaldi toolkit for evaluation tests.     
- `UFPAlign.py`: the UFPAlign2.0 GUI.    


## Citation

If you use any of the resources provided on this repository, please cite our
[BRACIS 2020 paper](https://link.springer.com/chapter/10.1007%2F978-3-030-61377-8_44)
as one of the following:

> Dias A.L., Batista C., Santana D., Neto N. (2020)
> Towards a Free, Forced Phonetic Aligner for Brazilian Portuguese Using Kaldi Tools.
> In: Cerri R., Prati R.C. (eds) Intelligent Systems. BRACIS 2020.
> Lecture Notes in Computer Science, vol 12319. Springer, Cham.
> https://doi.org/10.1007/978-3-030-61377-8_44

```bibtex
@InProceedings{Dias20,
    author     = {Dias, Ana Larissa and Batista, Cassio and Santana, Daniel and Neto, Nelson},
    editor     = {Cerri, Ricardo and Prati, Ronaldo C.},
    title      = {Towards a Free, Forced Phonetic Aligner for Brazilian Portuguese Using Kaldi Tools},
    booktitle  = {Intelligent Systems},
    year       = {2020},
    publisher  = {Springer International Publishing},
    address    = {Cham},
    pages      = {621--635},
    isbn       = {978-3-030-61377-8}
}
```

[![FalaBrasil](doc/logo_fb_github_footer.png)](https://ufpafalabrasil.gitlab.io/ "Visite o site do Grupo FalaBrasil") [![UFPA](doc/logo_ufpa_github_footer.png)](https://portal.ufpa.br/ "Visite o site da UFPA")

__Grupo FalaBrasil (2020)__ - https://ufpafalabrasil.gitlab.io/      
__Universidade Federal do Pará (UFPA)__ - https://portal.ufpa.br/     
Larissa Dias   - larissa.engcomp@gmail.com     
Cassio Batista - https://cassota.gitlab.io/    
Daniel Santana - daniel.santana.1661@gmail.com     
